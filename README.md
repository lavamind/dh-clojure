dh-clojure - debhelper extension to assist with Debian Clojure packaging
========================================================================

The `example-clojure/` directory contains a trivial dh-clojure based
package that provides an example, is handy for testing during
development, and is also used to test dh-clojure itself via
`dh_auto_test`.

Much of the code can be exercised/diagnosed via
```
debian/rules clean
fakeroot debian/rules binary
```
This will also build the `example-clojure` package.

dh-clojure internals
--------------------

Everything is driven by the `leiningen.pm` debhelper "buildsystem"
(see `/usr/share/doc/debhelper/PROGRAMMING.md.gz`).  It implements
subroutines for `dh_auto_*` packaging command sequences for building
Leiningen-based Clojure projects.

`debian/dh-clojure/` is reserved for dh-clojure's use.  If possible,
keep all of the paths that dh-clojure generates during the build in
`debian/dh-clojure/tmp` so that `dh_clojure_clean` only has to remove
that directory, and for now, an empty `debian/dh-clojure` will always
be removed on clean.

Certain environment variables are also modified during the build:

 - `LEIN_OFFLINE` is set to `true` to ensure that `lein` doesn't
    attempt to retrieve any dependencies from the network

 - `LEIN_HOME` is set to `debian/dh-clojure/lein-home`
