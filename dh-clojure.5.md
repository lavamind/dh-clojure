dh-clojure(5) "@DEB_VERSION@" "Debhelper Clojure tools"

# NAME

dh-clojure - debhelper extensions for Clojure packages

# DESCRIPTION

*dh-clojure* is intended to help with the packaging of Clojure
projects.  The current version primarily helps with leiningen based
projects, but that should change over time.

See /usr/share/doc/dh-clojure/example/rules for a sample debian/rules file.

# USAGE

The rules file must include "--with clojure" in the dh invocation,
e.g.
```
%:
	dh $@ --with clojure
```

The rules file must "include /usr/share/dh-clojure/rules/preamble",
which currently overrides the PATH to include
/usr/share/dh-clojure/override/bin first in order to override lein
to ensure it stays offline, doesn't scribble outside the build
directory, etc.

Currently "--with clojure" causes all of the *dh-clojure* tools to
replace the corresponding dh_auto_\* commands in the dh build
sequence.  If you would like to invoke those commands, you can use an
override of the *dh-clojure* target, e.g.

```
override_dh_clojure_configure:
	dh_auto_configure
	dh_clojure_configure
```

# FILES

- /usr/share/doc/dh-clojure/example/rules - sample rules file
- /usr/share/doc/debhelper/PROGRAMMING.gz

# SEE ALSO
*dh_clojure_configure*(1)
*dh_clojure_build*(1)
*dh_clojure_test*(1)
*dh_clojure_clean*(1)
*debhelper*(7)
