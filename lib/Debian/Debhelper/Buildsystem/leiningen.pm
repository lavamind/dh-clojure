package Debian::Debhelper::Buildsystem::leiningen;

=head1 NAME

leiningen -- debhelper build system class for Clojure packages

=head1 DESCRIPTION

The leiningen build system for debhelper is designed to facilitate packaging
Leiningen-based Clojure projects for Debian.

It can be activated by passing the B<--buildsystem=leiningen> option to B<dh>,
in additon to B<--with javahelper> and B<--with maven_repo_helper> to activate
the required Debhelper sequences:

 %:
    dh $@ --buildsystem=leiningen --with javahelper

=head1 IMPLEMENTATION

Here is a brief description of how the leiningen build system implements each
debhelper build system stage:

=over

=item B<configure>

Sets up a debian/dh-clojure/tmp/maven-repo symlink pointing to
/usr/share/maven-repo for use by lein as a :local-repo.

=item B<build>

Creates a debian/dh-clojure/tmp/pom.xml for use in the debian/PACKAGENAME.poms
file (which it also creates), and it creates an unversioned symlink in target/
for the jar.

=item B<install>

Calls C<mh_install> from the B<maven-repo-helper> package to install the built
jars into /usr/share/java and poms and jar symlinks in /usr/share/maven-repo.

=item B<test>

Runs C<lein test>.

=item B<clean>

Runs C<lein clean> and cleans up after the previous steps.

=back

=cut

use strict;
use warnings;
use parent qw(Debian::Debhelper::Buildsystem);
use Debian::Debhelper::Dh_Lib;

sub DESCRIPTION {
    'Leiningen'
}

sub DEFAULT_BUILD_DIRECTORY {
    'target'
}

sub check_auto_buildable {
    my $this = shift;
    return $this->get_sourcepath('project.clj') ? 0 : 1;
}

my $tmpdir = 'debian/dh-clojure/tmp';

sub new {
    my $class = shift;
    my $this  = $class->SUPER::new(@_);

    # set leiningen home under dh-clojure tmpdir
    $ENV{'LEIN_HOME'} = "$tmpdir/lein-home";

    # lein may not attempt to access the network
    # (Debian policy, section 4.9)
    $ENV{'LEIN_OFFLINE'} = 'true';

    return $this;
}

sub _get_project {
    my $this = shift;
    my ($property) = @_;

    my $cmd = "lein update-in :$property println -- version";
    my $output = `$cmd`;
    error_exitcode($cmd) if $?;

    my ($value) = split(/\n/, $output);
    return $value;
}

sub _do_lein {
    my $this = shift;
    my $local_repo = "$tmpdir/maven-repo";
    doit('/usr/bin/lein', 'update-in', ':', 'assoc', ':local-repo', "\"$local_repo\"", '--', @_);
}

sub configure {
    doit('mkdir', '-p', $tmpdir);
    doit('ln', '-s', '/usr/share/maven-repo', "$tmpdir/maven-repo");
}

sub build {
    my $this = shift;

    my $name = $this->_get_project('name');
    my $version = $this->_get_project('version');

    verbose_print("[META] Got project name: \"$name\"");
    verbose_print("[META] Got project version: \"$version\"");

    $this->_do_lein('pom', "$tmpdir/pom.xml");
    $this->_do_lein('jar');

    my $jar = "$name.jar";
    doit('test', '-f', "target/$name-$version.jar");
    doit({'chdir' => 'target'}, 'ln', '-s', "$name-$version.jar", "$jar");
    doit({ 'stdout' => "debian/lib$name-clojure.poms" },
           'echo',
           "$tmpdir/pom.xml",
           "--artifact=target/$jar",
           "--usj-name=$name",
           '--java-lib');
}

sub install {
    doit('mh_install');
}

sub test {
    my $this = shift;
    $this->_do_lein('test');
}

sub clean {
    my $this = shift;
    my $name = $this->_get_project("name");

    $this->_do_lein('clean');
    doit('rm', '-f', "debian/lib$name-clojure.poms");
    doit('rm', '-rf', $tmpdir);
    doit('rmdir', '--ignore-fail-on-non-empty', 'debian/dh-clojure');
}

1;
